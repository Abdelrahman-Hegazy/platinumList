<!DOCTYPE html>
<html>
<body>

<h1>Available Tables</h1>
<ul>
<?php

error_reporting(E_ALL);

// Alternatively, set it in your php.ini configuration file
// for a more persistent solution
ini_set('display_errors', 'On');
// Database credentials
$servername = "{{host}}";
$username = "{{db_user}}";
$password = "{{db_pass}}";
$dbname = "{{dp_name}}";
// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
} else {
  echo "Connection successful!<br>"; // Success message
}


// Close connection
mysqli_close($conn);


?>
</ul>

</body>
</html>

