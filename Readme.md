# PlatinumList
    test task forPlatinumList


## Steps  
- create local vm for jenkins   
    ``` kind cluster create```  
    ``` helm repo add jenkins https://charts.jenkins.io```  
    ```helm repo update```   
    ```helm install jenkins jenkins/jenkins```  

- create local vm for application  
- install php,mysql,httpd  
- created repo and pushed sample index.php app.
- setup jenkins to pull latest code every one minute (please find the attached screenshots 1-3 .png)
- Implement automated build and test steps in the Jenkins pipeline  
    for the sake of simplicity used the internal linting mode of php with php -l.
- deployed the application using jenkins as shown in jenkins file.
- attached screenshot (4.png for a successfull run, and 5.png for the app running)
- attached a sample of the last successfull job output in the repo itself.




## security and compliance 

- theoritically made sure the app is compliant with the linting rules,  
    but for the sake of the simplicity I did only used "php -l" 
- for simplicity I didn't configure https for the application , but in real world https configuration is a must.
- after mysql installation I used /usr/bin/mysql_secure_installation  to secure mysql installation by removing the anon users , delete unused DBs, disable root connection from outside the machine.
- for the sake of the security created a token the repo , so jenkins can use this token to connect to the gitlab.(screenshot 6.png)

- created firewall rules on the application server to only accept connection on certain port (80 , should be 443 but https not configured as mentioned)  
```firewall-cmd --zone=public --add-port=80/tcp --permanent```  
```firewall-cmd --reload```  
- for the sake of simplicity I only used built-in credentials manager in jenkins to save the secrets. (screenshot 8.png)





### how to make the app work locally 


- install httpd,mysql-server,php,php-fpm,php-mysqli  
- make sure httpd,php-fpm,mysqlserver services are active (systemctl)
- use mysql_secure_installation script to secure your installation and create root password 
- create a new user and a new DB for the application 
- put the index.php in the document root of httpd (/var/www/html may differ based on the os and the versions)

- go to the browser and test the app .



### how would it be in real life 

- higher level linters should be used and the rules should be customised , also we can use static code analysis tool like sonarqube and integrate it with jenkins.
    https://www.linkedin.com/advice/3/what-most-effective-tools-php-code-quality-standards-kfgac#:~:text=1-,2%20Code%20Linters,PHPStan%2C%20Psalm%2C%20and%20Phan. 

- Https should be configured , the simplist way is to use cert-bot to generate and maintan ssl certificates with let'sencrypt.

- the repo should be private , but in this case just made it public because I'll share the link to it. 
- using external secrets manager would be more secure and best practice , 
for example using hashicorp vault and integrating it with jenkins. https://plugins.jenkins.io/hashicorp-vault-plugin/




- moreover , all of this could be acheived using K8s and helm with less effort but I went with this way to follow the task discription.


### how jenkins file work 

this jenkins file is consisting of 3 main stages  using declarative approach.


- running php-linter

    - (line 12-22 ) running php lint command if this command failed the exit code will not be zero , so it will fial the stage and skip the next stage , and not deploy the application.
    - this should be more complex and other tools to be involved and more complex checks added  to make sure the app is compliant and secure.

- transfering the files  
    - (line 25-39 ) replacing the placeholders inside the index.php file to replace it with the real credentials for safty in order not to save any secrets on the repo. 

    - (line 42-48 ) moving the needed files to the correct destination on the server 


-  (line 50-61 )reloading httpd to make sure all changes are reflected ,


### Please note 

- this is very simple and in real prod it shouldn't be like that , 
- for the case of not complexting and the limitation of time this may be not very secure and production wise solution.



